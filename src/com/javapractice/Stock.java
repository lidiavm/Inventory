package com.javapractice;

public class Stock {
	
	private String productcode;
	private int amount;
	private int maxdeliverytime;
	
	public Stock(String productcode, int amount){
		this.setProductcode(productcode);
		this.setAmount(amount);
		if(productcode=="P"){
			this.setMaxdeliverytime(2);
		}else if(productcode=="T"){
			this.setMaxdeliverytime(2);
		}else{
			this.setMaxdeliverytime(3);
		}	
	}
	
	public void updateStock(){}

	public String getProductcode() {
		return productcode;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getMaxdeliverytime() {
		return maxdeliverytime;
	}

	public void setMaxdeliverytime(int maxdeliverytime) {
		this.maxdeliverytime = maxdeliverytime;
	}

}
