package com.javapractice;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;

/**
 * Created by jorge on 18/7/16.
 */
public class InventoryUpdater {

    public static void main(String[] args) {
        String stockPath = args[0];
        String ordersPath = args[1];    

        try {
            String stockContent = FileUtils.loadFile(stockPath);
            System.out.println(stockContent);    
            loadStock(stockContent);      
        } catch (FileNotFoundException e) {
            System.out.println("Error while loading stock: [" + stockPath + "]");
            e.printStackTrace();
        }

        try {
            String orderContent = FileUtils.loadFile(ordersPath);
            System.out.println("----------------------------------");
            System.out.println(orderContent);
            loadOrders(orderContent);
        } catch (FileNotFoundException e) {
            System.out.println("Error while loading orders: [" + ordersPath + "]");
            e.printStackTrace();
        }

    }
    
    public static void loadStock(String stockContent) {
    
	    final String LINE_SEPARATOR = System.getProperty("line.separator");
	    StringTokenizer st1 = new StringTokenizer( stockContent, LINE_SEPARATOR );
	    
	    ArrayList<Stock> StockList = new ArrayList<Stock>();
	    Hashtable<String, Integer> StockHt = new Hashtable<String, Integer>();
	
	    while (st1.hasMoreTokens())
	    {
	       String product = st1.nextToken();
	       System.out.println(product);
	       
	       StringTokenizer st2 = new StringTokenizer( product, ";" );
	       int i=0;
	       String orderArray[] = new String[2];
	       
	       while (st2.hasMoreTokens()){
	    	   orderArray[i] = st2.nextToken();
	    	   System.out.println(orderArray[i]);
	    	   i++;
	       }
	       
	       System.out.println("----------------------------------");
	       
	       String productcode = orderArray[0];
	       int amount = Integer.parseInt(orderArray[1]);
	       
	       Stock stockObject = new Stock(productcode, amount);
	       StockList.add(stockObject);
	       
	       StockHt.put(productcode, amount);
	    }
    }
    
    public static void loadOrders(String orderContent) {
        
        String lineSeparator = System.getProperty("line.separator");
        StringTokenizer st1 = new StringTokenizer( orderContent, lineSeparator );
        
        ArrayList<Orders> OrdersList = new ArrayList<Orders>();

        while (st1.hasMoreTokens())
        {
           String order = st1.nextToken();
           System.out.println(order);
           
           StringTokenizer st2 = new StringTokenizer( order, ";" );
           int i=0;
           String orderArray[] = new String[4];
           
           while (st2.hasMoreTokens()){
        	   orderArray[i] = st2.nextToken();
        	   System.out.println(orderArray[i]);
        	   i++;
           }
           
           System.out.println("----------------------------------");
           

           int ordernumber = Integer.parseInt(orderArray[0]);
           int customercode = Integer.parseInt(orderArray[1]);
           String productcode = orderArray[2];
           int amountordered = Integer.parseInt(orderArray[3]);

           Orders orderObject = new Orders(ordernumber, customercode, productcode, amountordered);
           OrdersList.add(orderObject);
        }
    }
    

}
