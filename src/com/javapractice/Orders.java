package com.javapractice;

public class Orders {

	private int ordernumber;
	private int customercode;
	private String productcode;
	private int amountorder;
	
	public Orders(int ordernumber, int customercode, String productcode, int amountorder){
		this.ordernumber = ordernumber;
		this.customercode = customercode;
		this.productcode = productcode;
		this.amountorder = amountorder;
	}
	
	public void ordersToDeliver(){}
	public void ordersPending(){}
	public void shipOrders(){}

	public int getOrdernumber() {
		return ordernumber;
	}

	public void setOrdernumber(int ordernumber) {
		this.ordernumber = ordernumber;
	}

	public int getCustomercode() {
		return customercode;
	}

	public void setCustomercode(int customercode) {
		this.customercode = customercode;
	}

	public String getProductcode() {
		return productcode;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public int getAmountorder() {
		return amountorder;
	}

	public void setAmountorder(int amountorder) {
		this.amountorder = amountorder;
	}
}
