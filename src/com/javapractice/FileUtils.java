package com.javapractice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by jorge on 18/7/16.
 */
public class FileUtils {

    /**
     * Loads content of file corresponding to given path and returns its content as a String
     * @param path relative path or absolute path of a text file
     * @return the content of the file with given path, as a String
     * @throws FileNotFoundException
     */
    public static String loadFile(String path) throws FileNotFoundException {
        File file = new File(path);
        StringBuilder fileContents = new StringBuilder((int)file.length());
        Scanner scanner = new Scanner(file);
        String lineSeparator = System.getProperty("line.separator");

        try {
            while(scanner.hasNextLine()) {
                fileContents.append(scanner.nextLine() + lineSeparator);
            }
            return fileContents.toString();
        } finally {
            scanner.close();
        }
    }

    /**
     * Creates or overwrites file corresponding to given path, and write content in the file.
     * Returns true if content can be written to file, false otherwise
     * @param path relative path or absolute path
     * @param content the content to write in the file
     * @return true if content was written to file sucessfully, false otherwise.
     * @throws FileNotFoundException
     */
    public static boolean writeFile(String path, String content) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(path);
        out.println(content);
        return !out.checkError();
    }

}

