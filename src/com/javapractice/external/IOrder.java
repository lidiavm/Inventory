package com.javapractice.external;

/**
 * Created by jorge on 18/7/16.
 */
public interface IOrder {

    public int getOrderId();
    public int getCustomerCode();
    public String getProductCode();
    public int getAmount();
    public int getMaxDays();
}
